import React, { Fragment, useEffect, useState } from 'react';
import './App.css';
import { Container, Row, Col, Form, ListGroup, Tab } from "react-bootstrap"

interface ILocation {
  name: string;
  id: number;
}

interface LocationDetail {
  name: string;
  id: number;
  latitude: number;
  longitude: number
}

function App() {
  const [result, setResult] = useState<ILocation[]>([]);
  const [query, setQuery] = useState("");
  const [selected, setSelected] = useState<LocationDetail>()

  useEffect(() => {
    if (query && query.trim()) {
      const getItem = async () => {
        const resp = await fetch(`https://code-challenge-backend.herokuapp.com/locations?q=${query}`)
        if (!resp.ok) {
          alert("An error happend. API returned false. please try again")
          return;
        }

        const items: ILocation[] = await resp.json();
        setResult(items);
      }

      getItem();
    }
  }, [query])

  const changeQuery = (e: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(e.currentTarget.value);
  }

  const fetchInfo = async (id: number) => {
    const resp = await fetch(`https://code-challenge-backend.herokuapp.com/locations/${id}`)
    if (!resp.ok) {
      alert("An error happend. API returned false. please try again")
      return;
    }
    const item: LocationDetail = await resp.json();
    setSelected(item);
  }

  return (
    <div className="App" style={{ margin: 25 }}>
      <Container>
        <Row>
          <Col>
            <Form.Group>
              <Form.Control placeholder="Search..." value={query} onChange={changeQuery} />
            </Form.Group>

            <Tab.Container id="list-group-tabs-example" defaultActiveKey="#link1">
              <Row>
                <Col sm={4}>
                  <ListGroup>
                    {result.map(y =>
                      <Fragment key={y.id}>
                        <ListGroup.Item active={y.id === selected?.id} onClick={() => fetchInfo(y.id)}>{y.name}</ListGroup.Item>
                      </Fragment>
                    )}
                  </ListGroup>
                </Col>
                <Col sm={8}>
                  <Tab.Content>
                    {!!selected && (
                      <div>
                        <h4>{selected?.name}</h4>
                        <p>{`latitude: ${selected?.latitude}`}</p>
                        <p>{`longitude: ${selected?.longitude}`}</p>
                      </div>
                    )}
                  </Tab.Content>
                </Col>
              </Row>
            </Tab.Container>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
