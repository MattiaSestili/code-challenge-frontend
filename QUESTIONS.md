# Questions

Q1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```
In the console it will print a string "2" 
and after 100 millisecond will print a string "1"
Why? becouse setTimeout is a function that take a function which it will executed after acertain time (ms)
 

Q2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```
This function will print in the console numbers from 10 to 0.
it calls itself recursively and print a number in the console till the number you passed is equal to 10.

Q3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```
 * This assign d the value of d if it is not a falsey value otherwise 5
 * This approach does not work if you want to pass a falsey value such as
 * false, null, undefined, 0 or "".

Q4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```
  output of this function is 3
  this becouse foo use closure javascript feature which is a combination of a function bundled together
  we assign to bar foo with 1 which return a function.
  when we call bar(2) means we are passing b withouht 


Q5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```
  double(5, (result) => console.log(result))
  the function takes a value and a callback so when we call we double on the second parameter 
  we can print the callback result.